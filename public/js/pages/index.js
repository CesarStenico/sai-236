import { screen } from './screen.js'

class IndexWindown extends screen{

    super(templateIndex){
        this.template = templateIndex;
    }

    startScripts(){

    }
  
}

let template = `
<div class="row">
    <div class="col-sm">
        <div>Buttons</div>
        <div class="row">
            <button class="btn btn-primary">btn-primary</button>
            <button class="btn btn-secondary">btn-secondary</button>
        </div>
        <div class="row">
            <button class="btn btn-primary-scale">btn-primary-scale</button>
            <button class="btn btn-secondary-scale">btn-secondary-scale</button>
        </div>

    </div>
    <div class="col-sm">
        <div>Buttons 2</div>
        <div class="row">
            <div class="col-sm">
                <button class="btn btn-danger">danger</button>
            </div>
            <div class="col-sm">
                <button class="btn btn-success">success</button>
            </div>
            <div class="col-sm">
                <button class="btn btn-warning">warning</button>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="row">
            <button class="btn btn-animation-one">btn-animation-one</button>
            <button class="btn btn-animation-two">btn-animation-two</button>
        </div>
    </div>
</div>   
<div class="row">
        <div class="col-sm">
            <div>Inputs</div>
            <div class="row">
                <input type="text" class="input-search" id="cepInput1" placeholder="Input1">
            </div>
            <div class="row">
                <input type="text" class="input-search" id="cepInput2" placeholder="Input2">
            </div>                     
        </div>
        <div class="col-sm">
            <div>Cards</div>
            <div class="row">
                <div class="card-primary">
                    <div class="card-title">Titulo do Card</div>
                    <div class="card-content">   
                        <p>Bairro:</p>
                        <p>Cidade:</p> 
                        <p>Logradouro:</p> 
                        <p>Estado:</p>                                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div> Spinner </div>
            <div class="spinner"></div>
        </div>

    </div>
</div>
`
export let screenIndex = new IndexWindown(template);