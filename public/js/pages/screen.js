export class screen{

    constructor(templateIndex){
        this.template = templateIndex;
        this.scripts;
    }

    start(screen){
        screen.innerHTML =  this.template;
        this.startScripts();
    }

}