import { screen } from './screen.js';

class ConsultaWindown extends screen{

    super(templateConsulta){
        this.template = templateConsulta;
    }

    startScripts(){
        let btn1 = document.getElementById("pesquisarCep");
        btn1.addEventListener("click", ()=>{
            
            
            let responseDiv = document.getElementById("response");
            responseDiv.innerHTML = "";
            let cepValue = document.getElementById("cepInput").value;
            let url = "https://api.postmon.com.br/v1/cep/"+cepValue;
            
            axios.get(url)
                .then(response => {
                    this.showCardCep(response)
                })
                .catch(error => {
                    this.showCardError();
                });

            this.showSpinner();
        })
    }

    showCardCep(response){
        let data = response.data;
        let responseDiv = document.getElementById("response");
        let content = `
            <div class="card-primary">
                <div class="card-title">Dados encontrados:</div>
                <div class="card-content">   
                    <p>Bairro: ${data.bairro}</p>
                    <p>Cidade: ${data.cidade}</p> 
                    <p>Logradouro: ${data.logradouro}</p> 
                    <p>Estado: ${data.estado}</p>  
                    <p>CEP: ${data.cep}</p>                                      
                </div>
            </div>
        `;

        responseDiv.innerHTML = content;
    }

    showCardError(){
        let responseDiv = document.getElementById("response");
        let content = `
            <div class="card-title">Nenhum dado encontrado!:(</div>
        `;

        responseDiv.innerHTML = content;
    }

    showSpinner(){
        let responseDiv = document.getElementById("response");
        let content = `
            <div class="spinner"></div>
        `;

        responseDiv.innerHTML = content;
    }

}

let template = `
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <p>Pesquisar CEP</p>
            <input type="text" class="input-search" id="cepInput" placeholder="Digite aqui o cep">
            <button class="btn btn-primary" id="pesquisarCep">Pesquisar</button>
        </div>
        <div class="col-sm">
        </div>
    </div>
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm" id="response"></div>
        <div class="col-sm"></div>
    </div>
`;

export let screenConsulta = new ConsultaWindown(template);
