**Objetivo:** Praticar fundamentos de javascript ES6, especificamente (Classes, Promises, Modules). E consumir dados de API, exibir, ordenar dados, e tratar erros.

Criar um aplicativo usando ES6 com AJAX, escolha a lib (ex. axios) que achar melhor para consultar os dados de CEP.

Reutilizar e integrar os componentes SASS criados na prática anterior .

Consumir dados da API: https://api.postmon.com.br/v1/cep/{cep}

O componente deverá ter um campo para digitar o CEP.

Em seguida faça a consulta pelo CEP usando a API acima, e exiba os dados visualmente no HTML.

Tratar os dados.

Tratar os erros.

Atenção: Não usar elemento algum no index.html, crie todo o HTML via classes ou functions (javascript) e faça a adição via injeção no DOM.