const express = require('express');
const path = require('path')
const cors = require('cors');

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use('/public', express.static(__dirname + '/public'));
app.use((req, res, next) => {
	//Qual site tem permissão de realizar a conexão, no exemplo abaixo está o "*" indicando que qualquer site pode fazer a conexão
    res.header("Access-Control-Allow-Origin", "*");
	//Quais são os métodos que a conexão pode realizar na API
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    app.use(cors());
    next();
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/public/views/index.html'));
})

app.listen(port, ()=>{
    console.log(`Server online in ${port} !`);
})